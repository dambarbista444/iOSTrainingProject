//
//  ViewController.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/10/21.
//

import UIKit

class ViewController: UIViewController {
    
    var personalInfoLabel = UILabel()
    var nameTextField = UITextField()
    var genderTextField = UITextField()
    var ageTextField = UITextField()
    var addButton = UIButton()
    
    /* This dictionary part i was little confuse, how am i gonna seprate age but according to talk on session someone explained who did in this similar way but i still believe this is not a right way but in mean time i am chosing this way and want to learn from you what could be right way or may be i didn't fully understood the questions. thank you
    */
    private var personDict:[String: [Int]] = ["kids": [17], "youngAdults": [35], "adults": [55], "seniors": [65]]
    private var personalInformations:[Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        view.addSubview(personalInfoLabel)
        view.addSubview(nameTextField)
        view.addSubview(genderTextField)
        view.addSubview(ageTextField)
        view.addSubview(addButton)
        
        configureLayoutFromExtensions()
        setUpLayout()
    }
    
    
    // Binding the VC UI properties with extension menthods.
    private func configureLayoutFromExtensions() {
        
        personalInfoLabel.setUpPersonaInfoLabel(label: personalInfoLabel)
        nameTextField.setupNameTextField(textField: nameTextField)
        genderTextField.setupGenderTextField(textField: genderTextField)
        ageTextField.setupAgeTextField(textField: ageTextField)
        addButton.setUpAddButton(button: addButton, selector: #selector(addPersonalInfo), vc: self)
    }
    
    
    // Checking the gender of users
    private func checkGender() -> Gender {
        let gender = Gender.other
        if genderTextField.text == "Male" || genderTextField.text == "male" {
            return Gender.male
        } else if genderTextField.text == "Female" || genderTextField.text == "female" {
            return Gender.female
        }
        
        return gender
    }
    
    
    // Checking the age of users
    private func checkAge() -> Int? {
        if ageTextField.text == .none {
            return nil
        } else {
            return Int(ageTextField.text!) ?? nil
        }
    }
    
    
    // Here i am adding user info into *personalInformations by acendig order of *age and *name
    @objc private func addPersonalInfo() {
    
        let personInfo = Person(name: nameTextField.text!, gender: checkGender() , age: checkAge() ?? nil)
        
        personalInformations.append(personInfo) // Adding personal info into personalInformations
        separteAge(by: checkAge() ?? 0) // Adding ages to personDict
        
        // Sorting the Names and age
        personalInformations.sort {$0.age ?? 0 < $1.age ?? 0}
        personalInformations.sort {$0.name < $1.name }
        
        nameTextField.text = .none
        ageTextField.text = .none
        genderTextField.text = .none
        print(personDict)
    }
    
    // Accepts indexes and returns pairs of person from alphabetically sorted list
    private func getPersonalInfo(from index: Int) -> Person {
        return personalInformations[index]
    }
    
    
    /*
     dictionary with keys kids(1-17), young adults(18-35), adults (36-55), senior (>55) value being list if person accordingly.
   */
    private func separteAge(by age: Int) {
        
        for (key, values) in personDict {
            var newValues = values
            if key == "kids" && age <= 17 {
                newValues.append(age)
                print(newValues)
            } else if key == "youngAdults", age >= 18 && age <= 35  {
                newValues.append(age)
                print(newValues)
            } else if key == "adults", age >= 36 && age <= 55 {
                newValues.append(age)
                print(newValues)
            } else if key == "seniors" && age >= 55  {
                newValues.append(age)
                print(newValues)
                
            }
        }
    }
    
}





