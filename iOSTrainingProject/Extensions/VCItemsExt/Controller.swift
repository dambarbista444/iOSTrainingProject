//
//  Controller.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/11/21.
//

import Foundation
import UIKit

// This extension of ViewController  is setup constraints for for VC items
extension ViewController {
    
    public func setUpLayout() {
        
        personalInfoLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 100).isActive = true
        personalInfoLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        personalInfoLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        personalInfoLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        nameTextField.topAnchor.constraint(equalTo: personalInfoLabel.bottomAnchor,constant: 20).isActive = true
        nameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        nameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        genderTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor,constant: 20).isActive = true
        genderTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        genderTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        genderTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        ageTextField.topAnchor.constraint(equalTo: genderTextField.bottomAnchor,constant: 20).isActive = true
        ageTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        ageTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        ageTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addButton.topAnchor.constraint(equalTo: ageTextField.bottomAnchor,constant: 40).isActive = true
        addButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    
    
}
