//
//  TextFields.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/10/21.
//

import Foundation
import UIKit

extension UITextField {
    
   public func setupNameTextField(textField: UITextField) {
        textField.placeholder = "Enter Your name"
        textField.borderStyle = .roundedRect
        textField.tintColor = .black
        textField.backgroundColor = .cyan
        textField.translatesAutoresizingMaskIntoConstraints = false
    }
    
    
    public func setupGenderTextField(textField: UITextField) {
        textField.placeholder = "Enter Your gender"
        textField.borderStyle = .roundedRect
        textField.tintColor = .black
        textField.backgroundColor = .cyan
        textField.translatesAutoresizingMaskIntoConstraints = false
    }
    
    
    public func setupAgeTextField(textField: UITextField) {
        textField.placeholder = "Enter Your age"
        textField.borderStyle = .roundedRect
        textField.tintColor = .black
        textField.backgroundColor = .cyan
        textField.translatesAutoresizingMaskIntoConstraints = false
    }
    
}
