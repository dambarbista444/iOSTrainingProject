//
//  Buttons.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/10/21.
//

import Foundation
import UIKit

extension UIButton {
    
    func setUpAddButton(button: UIButton, selector: Selector, vc: UIViewController) {
        button.setTitle("ADD", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .black
        button.addTarget(vc, action: selector, for: .touchUpInside)
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
    }
}
