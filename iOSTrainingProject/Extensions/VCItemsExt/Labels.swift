//
//  Labels.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/10/21.
//

import Foundation
import UIKit

extension UILabel {
    
    func setUpPersonaInfoLabel(label: UILabel) {
        label.text = "Enter your personal Info"
        label.font = .boldSystemFont(ofSize: 30)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
    }
    
    
}
