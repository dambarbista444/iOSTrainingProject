//
//  VCConstraintsViewModel.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/11/21.
//

import Foundation
import UIKit

/// This ViewModel

//struct VCConstraintsViewModel {
//
//    let personalInfoLabel: UILabel
//    let nameTextField: UITextField
//    let ageTextField: UITextField
//    let genderTextField: UITextField
//    let addButton: UIButton
//
//    init(constraintsModel: VCConstraintsModel) {
//        self.personalInfoLabel = constraintsModel.personalInfoLabel
//        self.nameTextField = constraintsModel.nameTextField
//        self.ageTextField = constraintsModel.ageTextField
//        self.genderTextField = constraintsModel.genderTextField
//        self.addButton = constraintsModel.addButton
//    }
//    
//
//    public func setUpLayoutConstraints(view: UIView) {
//
//
//        personalInfoLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 100).isActive = true
//        personalInfoLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        personalInfoLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        personalInfoLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
//
//        nameTextField.topAnchor.constraint(equalTo: personalInfoLabel.bottomAnchor,constant: 20).isActive = true
//        nameTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        nameTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        nameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
//
//        genderTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor,constant: 20).isActive = true
//        genderTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        genderTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        genderTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
//
//        ageTextField.topAnchor.constraint(equalTo: genderTextField.bottomAnchor,constant: 20).isActive = true
//        ageTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        ageTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        ageTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
//
//        addButton.topAnchor.constraint(equalTo: ageTextField.bottomAnchor,constant: 40).isActive = true
//        addButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
//        addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
//        addButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
//    }
//}
