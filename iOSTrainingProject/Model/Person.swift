//
//  Person.swift
//  iOSTrainingProject
//
//  Created by Dambar Bista on 8/11/21.
//

import Foundation

public struct Person {
    let name: String
    let gender: Gender
    let age: Int?
}


public enum Gender {
    case male
    case female
    case other
}
